import entity.BaseOperation;
import entity.OperationObject;
import operation.Difference;
import operation.Division;
import operation.Multiply;
import operation.Summary;
import exception.NonExistOperationException;
import exception.WrongDataInputException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class Main {
    public static List<BaseOperation> getOperationsList(){
        List<BaseOperation> operations = new ArrayList<>();
        BaseOperation summary = new Summary("+", 1);
        BaseOperation multiply = new Multiply("*", 2);
        BaseOperation difference = new Difference("-", 1);
        BaseOperation division = new Division("/", 2);
        operations.add(summary);
        operations.add(multiply);
        operations.add(difference);
        operations.add(division);
        return operations;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        OperationObject operationObject = new OperationObject(getOperationsList());
        Calculator calculator = new Calculator(operationObject);

        String request = "";
        while(true){
            System.out.println("Input expression: ");
            request = scanner.nextLine();
            if(request.equals("end")){
                break;
            }
            try{
                Double result = calculator.getResult(request);
                System.out.println("Result: " + result);
            }catch (NonExistOperationException | WrongDataInputException | ArithmeticException e){
                System.out.println(e.getMessage());
            }

        }

    }
}
