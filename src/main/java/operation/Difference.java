package operation;

import entity.BaseOperation;

public class Difference  extends BaseOperation {
    public Difference(String construction, Integer priority) {
        super(construction, priority);
    }

    @Override
    public Double apply(Double first, Double second) {
        return first - second;
    }

    @Override
    public String getConstruction() {
        return super.getConstruction();
    }

    @Override
    public Integer getPriority() {
        return super.getPriority();
    }
}
