package exception;

public class NonExistOperationException extends Exception{
    public NonExistOperationException(String message) {
        super(message);
    }
}
