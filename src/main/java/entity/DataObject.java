package entity;

import exception.WrongDataInputException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataObject {
    private final static String OPERAND_REGEX = "[-]?[0-9]+(.[0-9]+)?";
    private final String requestString;

    private List<String> operations;
    private List<Double> operands;

    public DataObject(String requestString) throws WrongDataInputException {
        this.requestString = requestString;
        init();
    }

    private void init() throws WrongDataInputException {
        Pattern pattern = Pattern.compile(OPERAND_REGEX);
        Matcher matcher = pattern.matcher(requestString);
        operands = new ArrayList<>();
        operations = new ArrayList<>();
        try{
            while (matcher.find()) {
                operands.add(Double.parseDouble(matcher.group()));
            }
        }catch (NumberFormatException e){
            throw new WrongDataInputException("Check your input string: " + matcher.group());
        }
        String onlyOperationsRequest = requestString
                .replaceAll(OPERAND_REGEX, "")
                .replace("  ", ",")
                .replace(" ", "");
        operations.addAll(Arrays.asList(onlyOperationsRequest.split(",")));
        if(operands.size() <= operations.size()){
            throw new WrongDataInputException("You entered an extra operation or did not enter an operand");
        }
    }

    public String getRequestString() {
        return requestString;
    }

    public List<String> getOperations() {
        return operations;
    }

    public List<Double> getOperands() {
        return operands;
    }
}
