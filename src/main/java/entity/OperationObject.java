package entity;

import entity.BaseOperation;
import exception.NonExistOperationException;

import java.util.ArrayList;
import java.util.List;

public class OperationObject {
    private final List<BaseOperation> operations;

    public OperationObject(List<BaseOperation> operations) {
        this.operations = operations;
    }

    public BaseOperation findOperationByConst(String construction) throws NonExistOperationException {
        BaseOperation operation;
        for(BaseOperation item: operations){
            if(item.getConstruction().equals(construction)){
                operation = item;
                return operation;
            }
        }
        throw new NonExistOperationException("Unexist operation " + construction);
    }

    public List<BaseOperation> convertStringOperationsInObjects(List<String> stringOperations) throws NonExistOperationException {
        List<BaseOperation> objectOperations = new ArrayList<>();
        for(String operation: stringOperations){
            objectOperations.add(findOperationByConst(operation));
        }
        return objectOperations;
    }

    public Integer getMaxPriorityIndex(List<BaseOperation> operations){
        int maxIndex = Integer.MIN_VALUE;
        BaseOperation maxPriorityOperation = null;
        for(int i = 0; i < operations.size(); i++){
            if(maxPriorityOperation == null || maxPriorityOperation.getPriority() < operations.get(i).getPriority()){
                maxPriorityOperation = operations.get(i);
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public List<BaseOperation> getOperations() {
        return operations;
    }
}
