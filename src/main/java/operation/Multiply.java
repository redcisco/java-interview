package operation;

import entity.BaseOperation;

public class Multiply extends BaseOperation {
    public Multiply(String construction, Integer priority) {
        super(construction, priority);
    }

    @Override
    public Double apply(Double first, Double second) {
        return first * second;
    }

    @Override
    public String getConstruction() {
        return super.getConstruction();
    }

    @Override
    public Integer getPriority() {
        return super.getPriority();
    }
}
