package entity;

public abstract class BaseOperation {
    protected String construction;
    protected Integer priority;

    public BaseOperation(String construction, Integer priority) {
        this.construction = construction;
        this.priority = priority;
    }


    public abstract Double apply(Double first, Double second);

    public String getConstruction() {
        return construction;
    }

    public Integer getPriority() {
        return priority;
    }
}
