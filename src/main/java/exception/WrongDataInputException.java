package exception;

public class WrongDataInputException extends Exception {
    public WrongDataInputException(String message) {
        super(message);
    }
}
