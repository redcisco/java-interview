import entity.BaseOperation;
import entity.DataObject;
import entity.OperationObject;
import exception.NonExistOperationException;
import exception.WrongDataInputException;

import java.util.List;

public class Calculator{
    private final OperationObject operationObject;

    public Calculator(OperationObject operationObject) {
        this.operationObject = operationObject;
    }

    public Double getResult(String request) throws NonExistOperationException, WrongDataInputException {
        DataObject dataObject = new DataObject(request);
        List<BaseOperation> operationsMap = operationObject
                .convertStringOperationsInObjects(dataObject.getOperations());
        List<Double> operands = dataObject.getOperands();
        int mapSize = operationsMap.size();

        for(int i = 0; i < mapSize; i++){
            int currentIndex = operationObject.getMaxPriorityIndex(operationsMap);
            BaseOperation currentOperation = operationsMap.get(currentIndex);
            operands.set(currentIndex, currentOperation.apply(
                    operands.get(currentIndex), operands.get(currentIndex + 1)));
            operands.remove(currentIndex + 1);
            operationsMap.remove(currentIndex);
        }
        return operands.get(0);
    }
}