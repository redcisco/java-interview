package operation;

import entity.BaseOperation;

public class Summary extends BaseOperation {
    public Summary(String construction, Integer priority) {
        super(construction, priority);
    }

    @Override
    public Double apply(Double first, Double second) {
        return first + second;
    }
}
