package operation;

import entity.BaseOperation;

public class Division extends BaseOperation {
    public Division(String construction, Integer priority) {
        super(construction, priority);
    }

    @Override
    public Double apply(Double first, Double second) {
        if(second.equals(0.0)){
            throw new ArithmeticException("You are divide by zero!");
        }
        return first / second;
    }
}
